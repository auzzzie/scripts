


Set-AutodiscoverVirtualDirectory -Identity * -ExternalUrl “https://autodiscover.cvsdev.com/autodiscover/autodiscover.xml” 

Set-webservicesvirtualdirectory –Identity * -ExternalUrl “https://mail.cvsdev.com/ews/exchange.asmx” 
Set-oabvirtualdirectory –Identity * –ExternalUrl “https://mail.cvsdev.com/oab” 
Set-owavirtualdirectory –Identity * –ExternalUrl “https://mail.cvsdev.com/owa” 
Set-ecpvirtualdirectory –Identity * –ExternalUrl “https://mail.cvsdev.com/ecp” 
Set-ActiveSyncVirtualDirectory -Identity * -ExternalUrl "https://mail.cvsdev.com/Microsoft-Server-ActiveSync"s



Set-AutodiscoverVirtualDirectory -Identity * –internalurl “http://autodiscover.cvsdev.com/autodiscover/autodiscover.xml” 
Set-ClientAccessServer –Identity * –AutodiscoverServiceInternalUri “http://autodiscover.cvsdev.com/autodiscover/autodiscover.xml” 
Set-webservicesvirtualdirectory –Identity * –internalurl “http://mail.cvsdev.com/ews/exchange.asmx” 
Set-oabvirtualdirectory –Identity * –internalurl “http://mail.cvsdev.com/oab” 
Set-owavirtualdirectory –Identity * –internalurl “http://mail.cvsdev.com/owa” 
Set-ecpvirtualdirectory –Identity * –internalurl “http://mail.cvsdev.com/ecp” 
Set-ActiveSyncVirtualDirectory -Identity * -InternalUrl "http://mail.cvsdev.com/Microsoft-Server-ActiveSync"


## Move Exchange Database
Get-MailboxDatabase -Server "$SERVER" | Move-DatabasePath -EdbFilePath D:\Database\MbxDb01.edb -LogFolderPath E:\Database\Logs -Confirm:$false

## Rename Mailbox Database
Get-MailboxDatabase -Server $SERVER | Set-MailboxDatabase -Name "MbxDb01" -Confirm:$false

## Disable Circular Logging for Migration
Get-MailboxDatabase -Server $SERVER | Set-MailboxDatabase -CircularLoggingEnabled $false

## Generate CSR
New-ExchangeCertificate -FriendlyName "Exchange 2013" -GenerateRequest -PrivateKeyExportable $true -KeySize 2048 -SubjectName "C=CA,S=AB,L=Edmonton,O=ECCC,OU=IT,CN=mail.edmccc.net" -DomainName mail.edmccc.net,autodiscover.edmccc.net,webmail.edmccc.net | out-file csr.txt                                                                                    

## Enable Certificate
Get-ExchangeCertificate | Enable-ExchangeCertificate -Services IIS,POP,IMAP,STMP -Confirm:$false

## Set OWA Logon Format
Get-OwaVirtualDirectory -Server $SERVER | Set-OwaVirtualDirectory -LogonFormat username -DefaultDomain eccc.local

## Configure OA
Get-OutlookAnywhere -Server ECC-S-EX01 | Set-OutlookAnywhere -InternalHostname mail.edmccc.net -ExternalHostname mail.edmccc.net -InternalClientAuthenticationMethod Ntlm -ExternalClientAuthenticationMethod Basic -ExternalClientsRequireSsl $True -InternalClientsRequireSsl $True

## Set Default OAB
Get-MailboxDatabase -Server ECC-S-EX01 | Set-MailboxDatabase -OfflineAddressBook "\Default Offline Address Book (Ex2013)"

## Create Receive Connector

## Create Send Connector

## Assign Anonymous Permissions
Get-ReceiveConnector “Receive Connector Name” | Add-ADPermission -User “NT AUTHORITY\ANONYMOUS LOGON” -ExtendedRights “Ms-Exch-SMTP-Accept-Any-Recipient”