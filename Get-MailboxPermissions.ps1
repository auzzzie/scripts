## Send As:

# Get all relevant SendAs permissions, this can take a LONG time...
$SendAs =  Get-Mailbox -Resultsize Unlimited | Get-ADPermission | where {$_.ExtendedRights -match "Send-As" -and $_.IsInherited -eq $False -and $_.User -notlike "NT AUTHORITY\SELF" -and $_.User -notlike "S-1-5-*"}
# Save to CSV file in a useful format:
$SendAs | select Identity, @{N="MbxSMTP";E={($_.identity | Get-Recipient).PrimarySmtpAddress.toString()}}, User, @{N="TrusteeSMTP";E={($_.User | Get-Recipient).PrimarySmtpAddress.toString()}} |  Export-CSV .\OnPremSendAsPerms.csv -NoType
## Full Control:

# Get all relevant FullControl permissions, this can take a little time...
$FullCtrl = Get-Mailbox -Resultsize Unlimited | Get-MailboxPermission | where {$_.AccessRights -match "FullAccess" -and !$_.IsInherited -and !$_.Deny -and $_.User -notlike "NT AUTHORITY\SELF" -and $_.User -notlike "S-1-5-*" -and $_.User -notlike "*BESAdmin" -and $_.User -notlike "*BPSAdmin" -and $_.User -notlike "*YTTech" -and $_.User -notlike "*Backup" -and $_.User -notlike "*cvsinfotech" -and $_.User -notlike "*Administrator" -and $_.User -notlike "*Domain Admins"}
# Save to CSV in a useful format:
$FullCtrl | select Identity, @{N="MbxSMTP";E={($_.identity | Get-Recipient).PrimarySmtpAddress.toString()}}, User, @{N="UserSMTP";E={($_.User | Get-Recipient).PrimarySmtpAddress.toString()}} | Export-CSV .\OnPremFullAccessPerms.csv -NoType
## Forwards to:

# Get all relevant Forward-To info into a CSV file:
Get-Mailbox -Filter {ForwardingAddress -ne $null} | select PrimarySMTPAddress,  @{N="FwdToSMTP";E={($_.ForwardingAddress | Get-Recipient).PrimarySmtpAddress.toString()}}, DeliverToMailboxAndForward |  Export-CSV .\OnPremForwardTo.csv -NoType
